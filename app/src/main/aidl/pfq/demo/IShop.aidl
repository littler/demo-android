// IShop.aidl
package pfq.demo;

import pfq.demo.ipc.Product;
// Declare any non-default types here with import statements

interface IShop {

    Product buy();

    // in: 表示这个对象能够从客户端到服务端，但是作为返回值从服务端到客户端的话，数据不会传送过去
    void setProduct(in Product product);

}
