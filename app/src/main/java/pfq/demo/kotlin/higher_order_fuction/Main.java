package pfq.demo.kotlin.higher_order_fuction;

/**
 * 高阶函数，本质上其实是通过创建匿名类实现的
 */
public class Main {

    public static int num1AndNum2(int num1, int num2, Fun fun) {
        return fun.invoke(num1, num2);
    }

    interface Fun {
        int invoke(int num1, int num2);
    }

    static class PlusFun implements Fun {

        @Override
        public int invoke(int num1, int num2) {
            return num1 + num2;
        }
    }

    static class MinusFun implements Fun {

        @Override
        public int invoke(int num1, int num2) {
            return num1 - num2;
        }
    }

    public static void main(String[] args) {

        System.out.println(num1AndNum2(2, 1, new PlusFun()));
        System.out.println(num1AndNum2(2, 1, new MinusFun()));

        System.out.println(num1AndNum2(2, 1, new Fun() {
            @Override
            public int invoke(int num1, int num2) {
                return num1 + num2;
            }
        }));

        System.out.println(num1AndNum2(2, 1, new Fun() {
            @Override
            public int invoke(int num1, int num2) {
                return num1 - num2;
            }
        }));

    }
}
