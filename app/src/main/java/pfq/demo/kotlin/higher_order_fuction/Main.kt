package pfq.demo.kotlin.higher_order_fuction

// 定义一个高阶函数
fun function(num1: Int, num2: Int, operation: (Int, Int) -> Int): Int {
    return operation(num1, num2)
}

fun plus(num1: Int, num2: Int): Int {
    return num1 + num2
}

fun minus(num1: Int, num2: Int): Int {
    return num1 - num2
}

fun main(args: Array<String>) {
    // 可以传入函数
    println(function(2, 1, ::plus))
    println(function(2, 1, ::minus))
    // 也可以传入lambda表达式，其实也是函数，只不过是匿名函数，因为lambda表达式本来就是匿名函数的简写么
    println(function(2, 1) { n1, n2 -> n1 + n2 })
    println(function(2, 1) { n1, n2 -> n1 - n2 })


    val result = StringBuilder().build {
        // 因为此lambda是定义在StringBuilder中的，所以天然拥有StringBuilder的上下文，可以直接调用StringBuilder的append()方法
        append("Apple")
        append("Orange")
    }
    println(result.toString())
}

// 定义了一个高阶函数，注意函数类型参数的写法，前面加了ClassName.表示这个函数类型是定义在哪个类当中
fun StringBuilder.build(function: StringBuilder.() -> Unit): StringBuilder {
    function()
    return this
}