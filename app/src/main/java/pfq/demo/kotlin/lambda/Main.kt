package pfq.demo.kotlin.lambda

/**
 * 函数的一般写法
 */
// kotlin中的函数默认都是有返回值的，返回Any对象，意思就是即使没有显式得写出来，kotlin也会帮你加上去
// 但如果显式定义了返回值，那就一定要有return关键字 ，即使你定义的返回值类型也是Any
fun f(x: Int): Any {
    return Any()
}

val y = f(1)

/**
 * 函数的lambda写法
 */
// 写法1：lambda中不能写return，因为最后一行就是返回值
fun fLambda1(x: Int) = {
    val a = 1
    val b = 2
    Any()
}

// 写法2：就是将参数写到函数体里面
fun fLambda2() = { x: Int -> Any() }

// 写法3：
// 这种写法有点怪，但还是满足Kotlin的函数语法：fun fun_name(): return_vale_type = {function_body}
// 只是Lambda函数的返回值类型跟我们之前见过的都不一样，是一个类似表达式的写法，在kotlin中把这种返回值类型叫作Lambda类型
// 所以，万变不离其宗，人家这样的写法没毛病
// 在kotlin中可以把Lambda类型当做参数和返回值的类型，但却不能像Python那样直接将一个函数名作为参数或返回值
fun fLambda3(): (Int) -> Any = { x: Int -> Any() }

// 用法1：
val yLambda1 = fLambda1(1)
// 用法2：
val yLambda2 = fLambda2()(1)

// 用法2：
val yLambda3 = fLambda2().invoke(1)

// 另外lambda里不能写return，因为->后面的值就是返回值

/**
 * 参数与返回值都是Lambda类型的函数
 */
fun f2(x: (Int) -> Int): (Int) -> Int {
    return { it -> x(it) }
}

fun main(args: Array<String>) {
    f2 { a:Int->a+1}
}

//https://mp.weixin.qq.com/s/xWiVzijYDYK5XjTsYJb65g