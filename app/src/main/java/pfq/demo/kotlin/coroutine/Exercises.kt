package pfq.demo.kotlin.coroutine

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.concurrent.thread
import kotlin.coroutines.EmptyCoroutineContext

fun main(args: Array<String>) {

    exercise1()
}

// 开启一个协程，并在协程中打印当前线程名
fun exercise1() {
    println("Start---------------------------------")

    runBlocking {
        println("runBlocking, 当前线程名：${Thread.currentThread().name}")
    }

    GlobalScope.launch {
        println("GlobalScope, 当前线程名：${Thread.currentThread().name}")
    }

    thread {
        val coroutineScope = CoroutineScope(context = EmptyCoroutineContext)
        coroutineScope.launch {
            println("CoroutineScope, 当前线程名：${Thread.currentThread().name}")
        }
    }


    println("---------------------------------End")
}