package pfq.demo.kotlin.coroutine

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

// 测试下创建协程和创建线程的性能对比
// 创建100万个协程和100万个线程的时间做比较
// 结果: 100万个线程需要30s，100万个协程只需要1s，性能显而易见
fun main(args: Array<String>){

    createCoroutines()

    createThreads()

}

fun createThreads(){
    val startTime = System.currentTimeMillis()

    for(i in 1..100_0000){
        Thread().start()
    }

    println("线程创建总时间：${(System.currentTimeMillis() - startTime) / 1000.0}s")
}

fun createCoroutines(){
    val startTime = System.currentTimeMillis()

    for(i in 1..100_0000){
        GlobalScope.launch {  }
    }

    println("协程创建总时间：${(System.currentTimeMillis() - startTime)/ 1000.0}s")

}