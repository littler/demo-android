package pfq.demo.kotlin.coroutine

import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    println("--------------------------")
    // 一定要使用runBlocking，不然会出现协程还没执行完main就结束了，导致你的测试日志没有打印或打印不全
    runBlocking {

        val time = measureTimeMillis {
            println("开始计时：")
            val d1 = async {
                println("子协程${Thread.currentThread().name}：正在运行")
                returnNum1()
            }

            val d2 = async {
                println("子协程${Thread.currentThread().name}：正在运行")
                returnNum2()
            }

            println("等待结果")
            println("result: ${d1.await() + d2.await()}")
        }

        println("time: $time")

    }
}


suspend fun returnNum1(): Int {

    delay(1000L)
    println("returnNum1()执行了")
    return 1
}

suspend fun returnNum2(): Int {
    delay(2000L)
    println("returnNum2()执行了")
    return 2
}