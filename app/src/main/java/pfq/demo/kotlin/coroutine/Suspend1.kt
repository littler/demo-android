package pfq.demo.kotlin.coroutine

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

suspend fun getInfo(): String {
    delay(1000L)
    delay(2000L)
    delay(3000L)
    runBlocking {

        GlobalScope.launch {
            val result = withContext(Dispatchers.IO) {
                // 模拟请求
                delay(1000)
                "result"
            }

            // textView.text = result
        }

    }
    return "HelloWorld!"
}