package pfq.demo.kotlin.lesson9

/**
 * with、run、apply是Kotlin的标准函数，为啥叫标准，就是这些函数都是定义在Standard.kt，任何Kotlin代码都可以自由地调用所有的标准函数
 */
fun main(args: Array<String>) {

    val r1 = with(StringBuilder()) {
        append("Apple")
        append("、")
        append("Orange")
        append("、")
        append("Banana")
        toString()
    }

    // with默认返回最后一行
    println(r1)

    val r2 = StringBuilder().run {
        append("Apple")
        append("、")
        append("Orange")
        append("、")
        append("Banana")
        toString()
    }
    // run默认返回最后一行
    println(r2)


    val r3 = StringBuilder().apply {
        append("Apple")
        append("、")
        append("Orange")
        append("、")
        append("Banana")
    }
    // apply默认返回上下文对象
    println(r3.toString())
}