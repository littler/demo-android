package pfq.demo.kotlin.lesson7

/**
 * 条件判断
 * Created by pangfuqiang on 2018/4/6.
 */
class Main {

}

fun main(args: Array<String>) {
    // if
    var x: Int? = null
    val a = if (x != null) 1 else 2

    when (x) {
        1 -> println("1")
        2 -> println("2")
        else -> {
            println("else")
            println("else")
        }
    }

    var s = "abc"
    val res = when {
        x in 1..10 -> "cheap"
        s.contains("hello") -> "it's a welcome!"
        else -> ""
    }

    var i = 0
    if (i >= 0 && i <= 10)
        println(i)

    if (i in 0..10)
        println(i)

    for (i in 0..10)
        println(i)

    for (i in 10..0)
        println(i)

    for (i in 10 downTo 0)
        println(i)

    for (i in 1..4 step 2) println(i)


}