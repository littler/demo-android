package pfq.demo.kotlin.lesson5;

public class Main2 {

    public static void main(String[] args) {

        Utils.Companion.sayHello("");
        Utils.sayHelloRealStatic();

        Singleton.Companion.getInstance().sayHello();
        Singleton.sayHelloRealStatic();

        Singleton2.INSTANCE.sayHello();
        Singleton2.sayHelloRealStatic();

        // 调用Kotlin中的顶层方法，是一个静态方法
        MainKt.doSomething();

    }

}
