package pfq.demo.kotlin.lesson5

object Singleton2 {
    fun sayHello() {
        println("hello2")
        doSomething()
    }

    @JvmStatic
    fun sayHelloRealStatic() {

    }
}