package pfq.demo.kotlin.lesson5

/**
 * 用一个工具类来展示Kotlin的静态属性
 * Created by pangfuqiang on 2018/3/18.
 */
class Utils {

    companion object {
        val HELLO: String = "hello world!"

        fun sayHello(hello: String) {
            println(hello)
        }

        @JvmStatic
        fun sayHelloRealStatic() {

        }
    }
}