package pfq.demo.kotlin.lesson5

import android.view.View

/**
 * Created by pangfuqiang on 2018/3/18.
 */
class Main {

}

fun main(args: Array<String>) {
    Utils.sayHello(Utils.HELLO)
    Utils.sayHelloRealStatic()

    var view = View(null)
    view.setOnClickListener { }

    Singleton.getInstance().sayHello()
    Singleton.sayHelloRealStatic()

    Singleton2.sayHello()
    Singleton2.sayHelloRealStatic()
}

fun doSomething() {
    println("doSomething")
}