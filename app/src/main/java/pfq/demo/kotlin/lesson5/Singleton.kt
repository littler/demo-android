package pfq.demo.kotlin.lesson5

/**
 * 单例模式
 * Created by pangfuqiang on 2018/3/18.
 */
class Singleton private constructor() {
    companion object {
        fun getInstance(): Singleton {
            return Singleton()
        }

        @JvmStatic
        fun sayHelloRealStatic() {

        }
    }

    fun sayHello() {
        println("hello")
    }


}