package pfq.demo.kotlin

// kotlin中的可变与不可变
fun main() {
    /**
     *变量的可变与不可变
     */
    var a = ""
    a = "1"
    val b = ""
    //b = "1" 编译报错

    /**
     * 可变list与不可变list
     */
    // 不可变list，就是不能添加和删除元素
    val aList = listOf<String>()
    // aList.add("1") 编译报错，因为根本就没有add方法

    // 可变list，就是可以添加和删除元素
    val bList = mutableListOf<String>()
    bList.add("1")
    bList.remove("1")
}