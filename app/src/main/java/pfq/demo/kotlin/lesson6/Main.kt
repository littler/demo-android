package pfq.demo.kotlin.lesson6

/**
 * null类型检查
 * Created by pangfuqiang on 2018/4/6.
 */
class Main {

}

fun main(args: Array<String>) {
    val a: Int? = null
    a?.toString()


    val b: Int? = null
    b?.toString()
}