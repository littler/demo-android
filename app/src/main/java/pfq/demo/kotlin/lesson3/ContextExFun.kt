package pfq.demo.kotlin.lesson3

import android.content.Context
import android.widget.Toast

/**
 * 此处定义关于Context的扩展方法
 * Created by pangfuqiang on 2018/3/12.
 */
/**
 * 扩展函数
 */
fun Context.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration)
}