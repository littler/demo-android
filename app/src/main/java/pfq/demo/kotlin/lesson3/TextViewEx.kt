package pfq.demo.kotlin.lesson3

import android.widget.TextView

/**
 * Created by pangfuqiang on 2018/3/12.
 */


/**
 * 扩展函数也可以是一个属性
 */
public var TextView.abc: CharSequence
    get() = getText()
    set(value) = setText(value)