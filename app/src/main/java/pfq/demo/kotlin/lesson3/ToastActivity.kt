package pfq.demo.kotlin.lesson3

import android.app.Activity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast

/**
 * 扩展方法
 * Created by pangfuqiang on 2018/3/12.
 */
class ToastActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // 弹出一个Toast
        toast("Hello, World!")
        toast("Hello, World!", Toast.LENGTH_LONG)


        var textView = TextView(this)
        var content = textView.abc

    }

}

