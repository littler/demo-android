package pfq.demo.kotlin.lesson2

/**
 * Created by pangfuqiang on 2018/3/9.
 */
fun main(args: Array<String>) {
    var i: Int = 100
    var d: Double = i.toDouble()

    var b: Boolean = false or true
    var b2: Boolean = false and true

    var a: Any = "one two three"

    var p: Person = Person()
    p.name = "pangfuqiang"
    print(p.name)

    p.age = 27
    println(p.age!!)
}

class Main {

}