package pfq.demo.kotlin.lesson2

/**
 * Created by pangfuqiang on 2018/3/9.
 */
class Person {

    var name: String = ""
        get() = field.toUpperCase()
        set(value) {
            field = "name:$value"
        }

    var age: Int? = null
        set(value) {
            field = value ?: 21
        }
}