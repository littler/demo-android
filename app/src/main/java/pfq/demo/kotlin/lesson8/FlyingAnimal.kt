package pfq.demo.kotlin.lesson8

/**
 * 接口定义，与Java接口的区别是：
 * Java接口是很纯粹的，所有属性都是静态常量，所有方法都是抽象方法
 * 但在Kotlin中，就没有这些限制了，与类的唯一区别是接口是没有状态的，所以属性需要子类去重写，类需要去保存接口属性的状态
 * Created by pangfuqiang on 2018/4/11.
 */
interface FlyingAnimal {
    val wings: Wings
    fun fly() = wings.move()
}