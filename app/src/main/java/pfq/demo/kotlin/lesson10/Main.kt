package pfq.demo.kotlin.lesson10

import kotlinx.coroutines.*

fun main() {

    // 顶层协程，会随着应用程序的结束而结束
    GlobalScope.launch {
        println("codes run in coroutine scope")
        delay(1500)
        println("codes run in coroutine scope finished")
    }

    Thread.sleep(1000)

    // 创建一个协程作用域，但可以保证协程作用域内的所有代码和子协程都执行完毕，但会阻塞所在线程
    runBlocking {
        println("codes run in coroutine scope")
        delay(1500)
        println("codes run in coroutine scope finished")
    }

    runBlocking {
        // launch表示开启一个协程
        launch {
            println("launch1")
            delay(1000)
            println("launch1 finished")
        }

        launch {
            println("launch2")
            delay(1000)
            println("launch2 finished")
        }
    }

    val start = System.currentTimeMillis()
    runBlocking {
        repeat(100000) {
            launch {
                printDot()
            }
        }
    }
    val end = System.currentTimeMillis()
    println(end - start)


    runBlocking {
        // async和launch一样都是开启一个协程，但async可以有返回值
        val result = async {
            5 + 5
        }.await()// await()方法表示如果async代码块中的代码还没执行完，那么将会阻塞当前的协程，直到可以获得async函数的执行结果为止

        println(result)
    }

}

// suspend可以将任意函数声明成可挂起函数，这样才能在内部调用其他挂起函数，比如delay()
suspend fun printDot() {
    // 创建一个协程作用域，但只会阻塞当前协程，既不影响其他协程，也不影响任何线程
    coroutineScope {
        launch {
            println(".")
            delay(1)
        }
    }

}