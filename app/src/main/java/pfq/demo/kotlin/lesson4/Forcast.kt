package pfq.demo.kotlin.lesson4

import java.util.*

/**
 * Created by pangfuqiang on 2018/3/14.
 */
data class Forcast(val date: Date, val temperature: Float, val details: String)