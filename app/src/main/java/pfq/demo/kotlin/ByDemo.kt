package pfq.demo.kotlin

import kotlin.reflect.KProperty

interface Base {
    fun print()
}

class Subject : Base {
    override fun print() {

    }

}

class Proxy(base: Base) : Base by base

class ByDemo {

    val a: String by lazy {
        println("1111111")
        "a"
    }

    var b: String by BDelegate()
}

class BDelegate {
    operator fun getValue(byDemo: ByDemo, property: KProperty<*>): String {
        return "b"
    }

    operator fun setValue(byDemo: ByDemo, property: KProperty<*>, s: String) {

    }

}


fun main() {
    // 代理类
    val subject = Subject()
    val proxy = Proxy(subject)
}