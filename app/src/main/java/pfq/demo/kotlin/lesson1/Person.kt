package pfq.demo.kotlin.lesson1

import android.content.Context
import android.widget.Toast

/**
 * Created by pangfuqiang on 2018/3/4.
 */
class Person(name: String, age: Int) : Animal() {

    init {

    }

    fun eat(): String = "吃"

    fun toast(context: Context, content: String, duration: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(context, content, duration)
    }

}