package pfq.demo.kotlin.flow//package pfq.demo.kotlin.flow
//
//import kotlinx.coroutines.Dispatchers
//import kotlinx.coroutines.delay
//import kotlinx.coroutines.flow.buffer
//import kotlinx.coroutines.flow.collect
//import kotlinx.coroutines.flow.collectIndexed
//import kotlinx.coroutines.flow.collectLatest
//import kotlinx.coroutines.flow.debounce
//import kotlinx.coroutines.flow.filter
//import kotlinx.coroutines.flow.flatMapConcat
//import kotlinx.coroutines.flow.flatMapLatest
//import kotlinx.coroutines.flow.flatMapMerge
//import kotlinx.coroutines.flow.flow
//import kotlinx.coroutines.flow.flowOf
//import kotlinx.coroutines.flow.flowOn
//import kotlinx.coroutines.flow.fold
//import kotlinx.coroutines.flow.internal.NopCollector.emit
//import kotlinx.coroutines.flow.map
//import kotlinx.coroutines.flow.onEach
//import kotlinx.coroutines.flow.reduce
//import kotlinx.coroutines.flow.sample
//import kotlinx.coroutines.flow.zip
//import kotlinx.coroutines.runBlocking
//
//fun main(args: Array<String>) {
//    runBlocking {
//        testBuffer()
//    }
//}
//
///**
// * 转换
// */
//suspend fun testMap() {
//    val flow = flowOf(1, 2, 3, 4, 5)
//    flow.map {
//        it * it
//    }.collectLatest {
//        println(it)
//    }
//}
//
///**
// * 过滤
// */
//suspend fun testFilter() {
//    val flow = flowOf(1, 2, 3, 4, 5)
//    flow.filter {
//        it % 2 == 0
//    }
//
//    flow<String> {
//        emit("")
//    }.onEach {  }.collect{
//        println()
//    }
//
//}
//
///**
// * 遍历
// */
//suspend fun testOnEach() {
//    val flow = flowOf(1, 2, 3, 4, 5)
//    flow.onEach {
//        println(it)
//    }.map {
//        it * it
//    }.collect {
//
//    }
//}
//
///**
// * 防抖动
// */
//suspend fun testDebounce() {
//    flow {
//        emit(1)
//        emit(2)
//        delay(600)
//        emit(3)
//        delay(100)
//        emit(4)
//        delay(100)
//        emit(5)
//    }
//        .debounce(500)// 意思是只有两条数据之间的间隔超过500毫秒才能发送成功，例如：a、b之间间隔600毫秒，那么a能发送成功，但b不一定，因为得看b是不是最后一条数据或者b的下一条数据的发送间隔是否超过了500毫秒
//        .collect {
//            println(it)
//        }
//
//    /**
//     * 结果：2、5
//     * 分析：
//     * 1、2是连续发送的，时间间隔不到500毫秒，所以1没有发送成功
//     * 2、3，时间间隔超过了500毫秒，所以2可以发送成功
//     * 3、4，时间间隔不到500毫秒，所以3没有发送成功
//     * 4、5，同理，4没有发送成功
//     * 5，因为是最后一条数据，所以5可以发送成功
//     */
//}
//
///**
// * 采样
// */
//suspend fun testSample() {
//    flow {
//        var i = 0
//        while (true) {
//            i++
//            emit("发送一条弹幕: $i")
//        }
//    }
//        .sample(1000)
//        .flowOn(Dispatchers.IO)
//        .collect {
//            println(it)
//        }
//}
//
///**
// * 累积
// */
//suspend fun testReduce() {
//    val result = flow {
//        for (i in (1..100)) {
//            emit(i)
//        }
//    }// accumulator: 累计值 value：当前值
//        .reduce { accumulator, value ->
//            accumulator + value
//        }
//    println(result)
//}
//
//
///**
// * 有初始值的累计
// */
//suspend fun testFold() {
//    val result = flow {
//        for (i in ('A'..'Z')) {
//            emit(i.toString())
//        }
//
//    }.fold("26个英文字母：") { accumulator, value ->
//        accumulator + value
//    }
//    println(result)
//}
//
///**
// * 串联多个flow
// */
//suspend fun testFlatMapContact() {
//    flowOf(1, 2, 3)
//        .flatMapConcat {
//            flowOf("a$it", "b$it")
//        }
//        .collect {
//            println(it)
//        }
//}
//
///**
// * 合并多个flow，和flatMapConcat一样都是合并flow，但区别是不管顺序
// */
//suspend fun testFlatMapMerge() {
//    flowOf(300, 200, 100)
//        .flatMapMerge {
//            flow {
//                delay(it.toLong())
//                emit("a$it")
//                emit("b$it")
//            }
//        }
//        .collect {
//            println(it)
//        }
//
//    /**
//     * 输出结果：
//     * a100
//     * b100
//     * a200
//     * b200
//     * a300
//     * b300
//     */
//
//    /**
//     * 但如果用flatMapConcat合并的话，输出结果就是按顺序合并的：
//     * a300
//     * b300
//     * a200
//     * b200
//     * a100
//     * b100
//     */
//}
//
///**
// * 合并多个flow，但如果新数据到达后上一个数据还没有处理完，则会丢掉上一个数据，直接处理新数据
// * 这个操作符属于喜新厌旧型操作符
// */
//suspend fun testFlatMapLatest() {
//    flow {
//        emit(1)
//        delay(150)
//        emit(2)
//        delay(50)
//        emit(3)
//    }
//        .flatMapLatest {
//            flow {
//                delay(100)
//                emit("$it")
//            }
//        }
//        .collect { println(it) }
//}
//
///**
// * 并行合并，flatMap是串行
// */
//suspend fun testZip() {
//    val start = System.currentTimeMillis()
//    val flow1 = flow {
//        delay(3000)
//        emit("a")
//    }
//    val flow2 = flow {
//        delay(2000)
//        emit(1)
//    }
//
//    flow1.zip(flow2) { a, b ->
//        a + b
//    }.collect {
//        val end = System.currentTimeMillis()
//        println("Time cost: ${end - start}ms")
//    }
//}
//
///**
// * 背压：buffer，解决Flow流速不均匀的问题，生产者生产数据太快，消费者消费数据太慢
// */
//suspend fun testBuffer() {
//    flow {
//        emit(1)
//        delay(1000)
//        emit(2)
//        delay(1000)
//        emit(3)
//    }.onEach {
//        println("$it is ready")
//    }
//        .buffer()
//        .collect {
//            delay(1000)
//            println("$it is handled")
//        }
//}
//
//suspend fun testConflate() {
//    flow {
//        var count = 0
//        while (true) {
//            emit(count)
//            delay(1000)
//            count++
//        }
//    }
//}