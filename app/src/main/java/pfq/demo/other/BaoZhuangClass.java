package pfq.demo.other;

/**
 * 包装类
 */
public class BaoZhuangClass {
    public static void main(String[] args) {
        /**
         * 装箱的3种方式
         */
        Integer i1 = new Integer(1);

        Integer i2 = Integer.valueOf(1);

        // 自动装箱
        Integer i3 = 1;


        /**
         * 拆箱的2种方式
         */
        int ii1 = i1.intValue();

        // 自动拆箱
        int ii2 = i2;

    }
}
