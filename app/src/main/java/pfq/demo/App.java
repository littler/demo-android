package pfq.demo;

import android.app.Application;

import com.yd.demo.arouter.ARouter;

import pfq.demo.module.ModuleAIndex;
import pfq.demo.module.ModuleBIndex;
import pfq.demo.plugin.HookUtils;
import pfq.demo.plugin.ProxyActivity;

/**
 * Created by pangfuqiang on 2018/2/23.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        HookUtils hookUtil = new HookUtils(ProxyActivity.class, this);
        hookUtil.hookAms();

        ARouter.getInstance().add(new ModuleAIndex());
        ARouter.getInstance().add(new ModuleBIndex());

    }
}
