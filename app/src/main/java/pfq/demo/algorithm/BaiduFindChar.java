package pfq.demo.algorithm;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 来自百度的一道面试题：
 * 找出字符串："podsuvjgrjivouvsdlewfpkufx"中，出现次数最少的字母
 */
public class BaiduFindChar {
    public static void main(String[] args) {
        String data = "podsuvjgrjivouvsdlewfpkufx";
        // 先放入一个LinkedHashMap中
        LinkedHashMap linkedHashMap = new LinkedHashMap<String, Integer>();
        // 转成数组
        char[] dataCharArray = data.toCharArray();
        // 依次遍历数组
        for (int i = 0; i < dataCharArray.length; i++) {
            String dataItem = String.valueOf(dataCharArray[i]);
            if (linkedHashMap.get(dataItem) == null) {
                // 不存在，则初始化次数为0
                linkedHashMap.put(dataItem, 0);
            } else {
                // 存在，则取出次数，并加1后重新放入
                int dataItemCount = (int) linkedHashMap.get(dataItem);
                linkedHashMap.put(dataItem, ++dataItemCount);
            }
        }
        // 遍历LinkedHashMap，记录最小的次数的那条，最后输出这条的key即可
        Map.Entry minMapEntry = null;
        Iterator it = linkedHashMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            if (minMapEntry == null || (int) entry.getValue() < (int) minMapEntry.getValue()) {
                minMapEntry = entry;
            }
        }

        // 输出结果
        System.out.println("出现最少次数的字母为：" + minMapEntry.getKey());


    }
}
