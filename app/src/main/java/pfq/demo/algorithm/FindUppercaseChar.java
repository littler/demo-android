package pfq.demo.algorithm;

/**
 * 查找一段字符串中的大写字母的个数
 * 思路：
 * 1. 将字符串转成字符数组
 * 2. 遍历数组
 * 3. 判断if(array[i] > 'A'  && array[i] < 'Z') 即为大写字母
 * <p>
 * 考察：
 * 对ASCII码的了解
 */
public class FindUppercaseChar {
    public static void main(String[] args) {
        FindUppercaseChar findUppercaseChar = new FindUppercaseChar();
        System.out.println("count: " + findUppercaseChar.getUppercaseCount("AcEddddeeeWd"));
    }

    public int getUppercaseCount(String str) {
        if (str == null || str.isEmpty()) {
            return 0;
        }

        char[] chars = str.toCharArray();

        int count = 0;
        for (char c : chars) {
            if (c >= 'A' && c <= 'Z') {
                count++;
            }
        }

        return count;
    }
}
