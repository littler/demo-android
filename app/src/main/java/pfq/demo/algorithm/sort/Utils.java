package pfq.demo.algorithm.sort;

public class Utils {

    public static int[] data() {
        return new int[]{9, 7, 8, 3, 5, 2, 1, 4, 6};
    }

    /**
     * 交换数组中两个索引的值
     *
     * @param arr
     * @param aIndex
     * @param bIndex
     */
    public static void swap(int[] arr, int aIndex, int bIndex) {
        // 这个判断很重要，如果交换的index相等，则最后该index的值为0
        if (aIndex != bIndex) {
            arr[aIndex] = arr[aIndex] + arr[bIndex];
            arr[bIndex] = arr[aIndex] - arr[bIndex];
            arr[aIndex] = arr[aIndex] - arr[bIndex];
        }
    }


}
