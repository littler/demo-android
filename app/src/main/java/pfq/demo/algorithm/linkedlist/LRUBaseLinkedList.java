package pfq.demo.algorithm.linkedlist;

/**
 * 基于链表的LRU缓存淘汰策略的实现：
 * 我们维护一个有序单链表，越靠近链表尾部的结点是越早之前访问的。当有一个新的数据被访问时，我们从链表头开始顺序遍历链表。
 * 1.1 一个新数据进来，如果链表未满，则插入表头；
 * 1.2 如果链表已满，则先删除链尾数据，再将新数据插入表头
 * 2. 一个老数据进来，直接将该数据移到表头
 */
public class LRUBaseLinkedList {

    // 链表长度
    private int length = 5;

    public static void main(String[] args) {

    }
}
