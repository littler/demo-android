package pfq.demo.algorithm;

import java.util.Arrays;

/**
 * 字符串排序
 * 使用的java的工具类
 * Arrays.sort()，内部根据数量的大小会选择相应的排序算法
 */
public class SortChar {
    public static void main(String[] args) {
        SortChar SortChar = new SortChar();
        System.out.println("count: " + SortChar.sort("AcEddddeeeWd"));
    }

    public String sort(String str) {
        if (str == null || str.isEmpty()) {
            return str;
        }

        char[] chars = str.toCharArray();

        Arrays.sort(chars);

        return new String(chars);
    }
}
