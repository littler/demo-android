package pfq.demo.algorithm.array;

/**
 * 在数组中插入数据
 */
public class Practice4 {

    public static void insert(int[] a, int index, int data) {

        if (a == null || index >= a.length) {
            return;
        }

        // 把index之后的数据都向后移动一位
        for (int i = a.length - 1; i > index; i--) {
            a[i] = a[i - 1];
        }

        // 将数据插入index位置
        a[index] = data;

    }

    public static void print(int[] a) {
        if (a == null || a.length == 0) {
            System.out.println("数组为空");
            return;
        }

        System.out.print("数组中的数据：");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }

        System.out.println();
    }

    public static void main(String[] args) {
        int[] a = {5, 2, 6, 4, 1, 6, 0};

        print(a);

        insert(a, 2, 10);

        print(a);

    }

}
