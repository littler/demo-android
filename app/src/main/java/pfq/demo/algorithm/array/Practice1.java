package pfq.demo.algorithm.array;

/**
 * 面试题：
 * 编写一个函数，删除一个整型数组中的重复元素，例如一个数组中的元素为{5, 6, 7, 6}，删除重复元素后数组变为{5, 6, 7}
 */
public class Practice1 {

    // 数组长度
    static int length;

    public static void main(String[] args) {

        int[] a = {6, 5, 7, 6};
        length = a.length;

        print(a);

        delRepeat(a);

        print(a);

    }

    public static void print(int[] a) {
        if (a == null || length == 0) {
            System.out.println("数组为空");
            return;
        }

        System.out.print("数组中的数据：");
        for (int i = 0; i < length; i++) {
            System.out.print(a[i] + " ");
        }

        System.out.println();
    }

    public static void delRepeat(int[] a) {
        if (a == null || length == 0) {
            return;
        }

        for (int i = 0; i < length - 1; i++) {

            for (int j = i + 1; j < length; j++) {

                if (a[i] == a[j]) {
                    delOne(a, j);
                }
            }
        }
    }

    /**
     * 删除数组中的某个元素，就是将该元素之后的所有元素依次向前移动一位
     */
    public static void delOne(int[] a, int index) {
        if (a == null || length == 0 || index < 0 || index >= length) {
            return;
        }

        System.out.println("要删除的数据是：" + a[index] + ", 下标为：" + index);
        for (int i = index + 1; i < length; i++) {
            a[i - 1] = a[i];
        }

        length--;
    }
}
