package pfq.demo.algorithm.array;

/**
 * 约瑟夫环的问题：
 * 古代有一个叫约瑟夫的犹太历史学家，他和一个朋友以及其他总共41个人被敌人追杀，
 * 被迫躲在了一个山洞里，其他人商量决定自杀也不被敌人抓住，于是就提出一个自杀的方式，
 * 就是这41个人围成一个圆圈，然后从第一个人开始报数，每报数到3的时候，这个人就必须自杀，
 * 然后从下一个人开始继续从1开始报数，直到所有人都自杀身亡为止。
 * 然而约瑟夫和他的朋友并不想自杀，于是让其朋友假装答应，并偷偷把朋友安排在第16个，把自己安排在第31个位置，
 * 最后他俩都成功逃过了这场死亡游戏。
 */
public class Practice3 {

    public static void main(String[] args) {

        // 总人数
        final int peopleSum = 41;
        // 报数到几时自杀
        final int badNum = 3;
        // 下标表示编号，值表示自杀的顺序
        int[] a = new int[peopleSum];

        int count = 1;

        int index = -1;

        // 开始
        while (count <= peopleSum) {

            int num = 1;

            do {

                index = ++index % peopleSum;

                if (a[index] == 0) {
                    num++;
                }

            } while (num <= badNum);

            a[index] = count;
            count++;
        }

        // 打印数组
        for (int i = 0; i < a.length; i++) {
            System.out.println("编号：" + (i + 1) + ", 自杀顺序：" + a[i]);
        }

    }

}
