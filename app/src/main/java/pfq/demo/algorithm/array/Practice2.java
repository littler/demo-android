package pfq.demo.algorithm.array;

/**
 * 编程实现数组的就地逆置，即利用数组的存储空间将数组中的元素倒置
 *
 * 思路：
 * 1. 使用两个指针，一个low指向数组第一个元素的下标，一个high指向数组的最后一个元素的下标；
 * 2. 将low和high指向的元素进行交换，此处交换不用使用临时变量
 * 3. low++，high--
 * 4. 使用while判断low<high
 * 结束
 *
 */
public class Practice2 {
    public static void main(String[] args) {
        int[] a = {9, 8, 7, 6, 5, 4, 3, 2, 1};

        print(a);

        reverse(a);

        print(a);
    }

    public static void reverse(int[] a) {

        if (a == null || a.length <= 1) {
            return;
        }

        int i = 0;
        int j = a.length - 1 - i;

        while (i < j) {
            // 交换
            a[i] = a[i] + a[j];
            a[j] = a[i] - a[j];
            a[i] = a[i] - a[j];

            // 继续
            i++;
            j--;
        }
    }

    public static void print(int[] a) {
        if (a == null || a.length == 0) {
            System.out.println("数组为空");
            return;
        }

        System.out.print("数组中的数据：");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }

        System.out.println();
    }
}
