package pfq.demo.bezier;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

/**
 * 二阶贝塞尔曲线
 * 思路：
 * 1. 绘制两个数据点
 * 2. 绘制一个控制点
 * 3. 绘制二阶贝塞尔曲线，通过Path的quadTo方法
 */
public class MyBezier extends View {

    private Paint mPointPaint;
    private Paint mLinePaint;
    private Paint mBezierPaint;
    // 两个数据点的位置
    private PointF mStartPoint;
    private PointF mEndPoint;
    // 一个控制点的位置
    private PointF mControlPoint;


    public MyBezier(Context context) {
        super(context);
    }

    public MyBezier(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mStartPoint = new PointF(0, 0);
        mEndPoint = new PointF(0, 0);
        mControlPoint = new PointF(0, 0);

        mPointPaint = new Paint();
        mPointPaint.setColor(Color.BLACK);
        mPointPaint.setStrokeWidth(8);
        mPointPaint.setStyle(Paint.Style.STROKE);
        mPointPaint.setTextSize(60);

        mLinePaint = new Paint();
        mLinePaint.setColor(Color.GRAY);
        mLinePaint.setStrokeWidth(6);

        mBezierPaint = new Paint();
        mBezierPaint.setColor(Color.RED);
        mBezierPaint.setStrokeWidth(6);
        mBezierPaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);


        mStartPoint.x = w / 2 - 200;
        mStartPoint.y = h / 2;

        mEndPoint.x = w / 2 + 200;
        mEndPoint.y = h / 2;

        mControlPoint.x = mStartPoint.x + (mEndPoint.x - mStartPoint.x) / 2;
        mControlPoint.y = mEndPoint.y - 100;

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // 绘制辅助线
        canvas.drawLine(mStartPoint.x, mStartPoint.y, mControlPoint.x, mControlPoint.y, mLinePaint);
        canvas.drawLine(mEndPoint.x, mEndPoint.y, mControlPoint.x, mControlPoint.y, mLinePaint);

        // 绘制3个点
        canvas.drawPoint(mStartPoint.x, mStartPoint.y, mPointPaint);
        canvas.drawPoint(mEndPoint.x, mEndPoint.y, mPointPaint);
        canvas.drawPoint(mControlPoint.x, mControlPoint.y, mPointPaint);

        // 绘制贝塞尔曲线
        Path path = new Path();

        path.moveTo(mStartPoint.x, mStartPoint.y);
        path.quadTo(mControlPoint.x, mControlPoint.y, mEndPoint.x, mEndPoint.y);

        canvas.drawPath(path, mBezierPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        mControlPoint.x = event.getX();
        mControlPoint.y = event.getY();
        invalidate();

        return true;


    }
}
