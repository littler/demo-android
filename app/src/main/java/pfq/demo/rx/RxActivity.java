package pfq.demo.rx;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import pfq.demo.R;


public class RxActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rx);

        findViewById(R.id.activity_rx_updateui_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RxActivity.this, UIUpdateActivity.class));
            }
        });

        findViewById(R.id.activity_rx_search_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RxActivity.this, SearchOptActivity.class));
            }
        });

        findViewById(R.id.activity_rx_polling_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RxActivity.this, PollingActivity.class));
            }
        });

    }

}
