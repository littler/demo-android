package pfq.demo.java;

/**
 * 构造代码块：定义在类中的代码块
 * <p>
 * 代码块在Java中分为三种：
 * 1. 静态代码块：类加载的时候就会执行，而且只执行一次
 * 2. 构造代码块：定义在类中的代码块，执行优先于构造方法
 * 3. 方法代码块：定义在方法中的代码块
 * <p>
 * 父子类中代码块的执行顺序：静态代码块--->父类的构造代码块--->父类的构造函数--->子类的构造代码块--->子类的构造函数
 */
public class CodeBlock {

    static class Bike {

        static {
            System.out.println("我是静态代码块");
        }

        {
            System.out.println("我是构造代码块");
        }

        private String name;
        private String country;

        Bike() {
            System.out.println("我是构造方法");
        }


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }
    }

    public static void main(String[] args) {

        // 常规实例化方式
        Bike normalInstanceWay = new Bike();
        normalInstanceWay.setName("崔克");
        normalInstanceWay.setCountry("美国");


        // 匿名内部类的构造代码块实例化方式(又叫"双大括号初始化方式")
        // 下面代码其实是创建了一个Bike的匿名内部类(Bike的匿名子类)，然后新增了一个构造代码块，在构造代码块中调用set方法进行初始化
        Bike constructInstanceWay = new Bike() {
            {
                setName("捷安特");
                setCountry("中国台湾");
                System.out.println("我是子类构造代码块");
            }
        };

        System.out.println(constructInstanceWay.name);
        System.out.println(constructInstanceWay.country);

    }

}
