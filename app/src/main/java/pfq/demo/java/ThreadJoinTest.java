package pfq.demo.java;

public class ThreadJoinTest {
    public static void main(String[] args) {
        Thread t1 = new Thread() {
            @Override
            public void run() {
                super.run();
                System.out.println("线程1");
            }
        };

        Thread t2 = new Thread() {
            @Override
            public void run() {
                super.run();
                System.out.println("线程2");
            }
        };

        Thread t3 = new Thread() {
            @Override
            public void run() {
                super.run();
                System.out.println("线程3");
            }
        };

        Thread t4 = new Thread() {
            @Override
            public void run() {
                super.run();
                System.out.println("线程4");
            }
        };

        try {
            t1.start();
            t2.start();
            t3.start();

            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        t4.start();
    }
}
