package pfq.demo.java;

/**
 * 面试题：
 * 不借助String相关方法，把int型数据转成char[]。
 * 例如：2345转成'2','3','4','5'
 * <p>
 * 思路：
 * 2345 / 1000 % 10 = 2;
 * 2345 / 100 % 10 = 3;
 * 2345 / 10 % 10 = 4;
 * 2345 / 1 % 10 = 5;
 * <p>
 * 就是先除后求余数
 * <p>
 * 这里的关键是先计算出初始除数，另外还有如何将int转成char（把整数2转成字符'2'的方法是：char a = '0' + 2;不能用char a = '' + 2;因为在java中没有''，编译会报错）
 */
public class Interview {
    public static void main(String[] args) {
        char[] a = convert(2345);

        System.out.println(String.valueOf(a));
    }

    private static char[] convert(int num) {

        // 构造初始除数
        int m = 10;
        int len = 1;
        while (num / (m * 10) > 0) {
            m = m * 10;
            len++;
        }

        len++;

        char[] result = new char[len];

        System.out.println("m: " + m);
        System.out.println("len: " + len);

        final int n = 10;

        int i = 0;

        while (m >= 1) {
            result[i] = (char) ('0' + (num / m) % n);
            m = m / 10;

            System.out.println("char[" + i + "]: " + result[i]);

            i++;
        }

        return result;
    }
}
