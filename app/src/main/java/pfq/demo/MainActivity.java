package pfq.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.yd.demo.arouter.ARouter;

import pfq.demo.anim.AnimActivity;
import pfq.demo.anim.property.PropertyActivity;
import pfq.demo.architecture.mvvm.MvvmActivity;
import pfq.demo.bezier.BezierActivity;
import pfq.demo.flowtextview.FlowTextViewActivity;
import pfq.demo.handler.HandlerActivity;
import pfq.demo.imageview.ScaleTypeActivity;
import pfq.demo.ipc.IPCActivity;
import pfq.demo.okhttp.OkHttpActivity;
import pfq.demo.rx.RxActivity;
import pfq.demo.threads.HandlerThreadActivity;
import pfq.demo.threads.ThreadPoolActivity;
import pfq.demo.view.ActionCancleActivity;
import pfq.demo.view.ConstraintLayoutActivity;
import pfq.demo.view.CustomViewActivity;
import pfq.demo.view.DispatchEventActivity;
import pfq.demo.view.FrameLayoutActivity;
import pfq.demo.view.MergeActivity;
import pfq.demo.view.RequestLayoutInvalidateActivity;
import pfq.demo.view.ViewEventDispatchActivity;
import pfq.demo.xml.XmlParser;

//import pfq.demo.architecture.mvvm.MVVMActivity;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.activity_enter_exit_anim_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, AnimActivity.class));
            }
        });

        findViewById(R.id.activity_enter_mvvm_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MvvmActivity.class));
            }
        });

        findViewById(R.id.activity_enter_bezier_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, BezierActivity.class));
            }
        });

        findViewById(R.id.activity_enter_imagetextview_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FlowTextViewActivity.class));
            }
        });

        findViewById(R.id.activity_enter_propertyanimation_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, PropertyActivity.class));
            }
        });

        findViewById(R.id.activity_enter_ipc_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, IPCActivity.class));
            }
        });

        findViewById(R.id.activity_enter_rxjava_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, RxActivity.class));
            }
        });

        findViewById(R.id.module_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ARouter.getInstance().build("/module/a").navigation(MainActivity.this);
            }
        });

        findViewById(R.id.xml_parser_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    XmlParser.parse(getAssets().open("main_layout.xml"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        findViewById(R.id.custom_view_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, CustomViewActivity.class));
            }
        });

        findViewById(R.id.handler_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, HandlerActivity.class));
            }
        });

        findViewById(R.id.scaleType_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ScaleTypeActivity.class));
            }
        });

        findViewById(R.id.framelayout_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FrameLayoutActivity.class));
            }
        });

        findViewById(R.id.constraintlayout_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ConstraintLayoutActivity.class));
            }
        });

        findViewById(R.id.merge_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MergeActivity.class));
            }
        });

        findViewById(R.id.thread_pool_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ThreadPoolActivity.class));
            }
        });

        findViewById(R.id.view_event_dispatch_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ViewEventDispatchActivity.class));
            }
        });

        findViewById(R.id.view_event_dispatch_item2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, DispatchEventActivity.class));
            }
        });

        findViewById(R.id.action_cancel_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ActionCancleActivity.class));
            }
        });

        findViewById(R.id.handler_thread_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, HandlerThreadActivity.class));
            }
        });

        findViewById(R.id.okhttp_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, OkHttpActivity.class));
            }
        });

        findViewById(R.id.requestLayout_invalidate_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, RequestLayoutInvalidateActivity.class));
            }
        });

    }

}
