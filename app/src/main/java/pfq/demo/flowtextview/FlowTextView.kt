package pfq.demo.flowtextview

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.text.Layout
import android.text.StaticLayout
import android.util.AttributeSet
import android.widget.TextView
import pfq.demo.R


/**
 * 支持图文混排的TextView
 *
 * 暂时只支持图片在文本的左边的图文混排效果
 *
 * 思路：将文字分成两个部分：图片占用部分、图片未占用部分
 * 所以关键是如何分？
 * 关键：借助StaticLayout的getLineVisibleEnd(line)，该方法传入行数，会返回该行最后一个字符所在的索引，这样通过字符串截取，就可以得到图片占用部分的StaticLayout，图片未占用部分的StaticLayout同理
 */
class FlowTextView(context: Context?, attrs: AttributeSet?) : TextView(context, attrs) {

    private lateinit var mLeftImageBitmap: Bitmap

    init {
        attrs?.let {
            val imageResId = attrs.getAttributeResourceValue(R.mipmap.ic_launcher, R.mipmap.ic_launcher)
            mLeftImageBitmap = BitmapFactory.decodeResource(resources, imageResId)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)

        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)

        setMeasuredDimension(widthSize, heightSize)
        // 此处有个疑问，直接使用父类的onMeasure方法会
        //super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDraw(canvas: Canvas?) {

        canvas?.let {
            canvas.drawBitmap(mLeftImageBitmap, left.toFloat(), top.toFloat(), paint)

            // 将文字分成两个部分：1. 图片占用的部分  2. 图片未占用的部分

            // 1. 图片占用的部分
            var imageTextStaticLayout = StaticLayout(text.toString(), paint, right - left - mLeftImageBitmap.width, Layout.Alignment.ALIGN_NORMAL, 1.0F, 0.0F, true)
            // 测试一行文字的高度
            val oneLineStaticLayout = StaticLayout("高度", paint, right - left - mLeftImageBitmap.width, Layout.Alignment.ALIGN_NORMAL, 1.0F, 0.0F, true)
            // 图片的高度 除以 一行文字的高度 = 图片高度所占用的文字行数
            val imageUsedTextLines = Math.ceil(mLeftImageBitmap.height / oneLineStaticLayout.height.toDouble()).toInt()
            // 获取图片占用部分最后一行最后一个字符所在的索引----此处是关键
            val imageUserdLastCharacterOffset = imageTextStaticLayout.getLineVisibleEnd(imageUsedTextLines - 1)
            // 得到图片占用部分的StaticLayout
            imageTextStaticLayout = StaticLayout(text.toString().subSequence(0, imageUserdLastCharacterOffset), paint, right - left - mLeftImageBitmap.width, Layout.Alignment.ALIGN_NORMAL, 1.0F, 0.0F, true)


            // 2. 图片未占用的部分
            val onlyTextStaticLayout = StaticLayout(text.toString().subSequence(imageUserdLastCharacterOffset, text.toString().length), paint, right - left, Layout.Alignment.ALIGN_NORMAL, 1.0F, 0.0F, true)

            // 将画布左移
            canvas.save()
            canvas.translate((left + mLeftImageBitmap.width).toFloat(), 0f)
            imageTextStaticLayout.draw(canvas)
            canvas.restore()


            // 将画布上移
            canvas.save()
            canvas.translate(0f, mLeftImageBitmap.height.toFloat())
            onlyTextStaticLayout.draw(canvas)
            canvas.restore()
        }

    }
}