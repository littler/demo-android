package pfq.demo.designpattern.chainofresponsibilitypattern.activity;

public class View {

    String name;

    View(String name) {
        this.name = name;
    }

    public boolean dispatchTouchEvent(MotionEvent event) {
        System.out.println(name + ": dispatchTouchEvent");
        return onTouchEvent(event);

    }

    public boolean onTouchEvent(MotionEvent event) {
        System.out.println(name + ": onTouchEvent");
        // 如果返回true，表示消费，那事件到此结束
        // 如果返回false，表示不消费，事件会向上传到父控件的onTouchEvent()方法中
        return true;
    }

}
