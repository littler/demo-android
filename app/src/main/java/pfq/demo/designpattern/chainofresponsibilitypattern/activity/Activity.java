package pfq.demo.designpattern.chainofresponsibilitypattern.activity;

/**
 * 用Android中的事件分发机制来实战责任链模式，当然算是复杂版的责任链模式
 * <p>
 * 责任链模式的思想是多个对象依次对数据进行处理
 */
public class Activity {

    PhoneWindow mWindow = new PhoneWindow();

    public static void main(String[] args) {

        // 创建View树
        ViewGroup rootViewGroup = new ViewGroup("rootViewGroup");

        ViewGroup aViewGroup = new ViewGroup("aViewGroup");

        View view = new View("view");

        rootViewGroup.addView(aViewGroup);

        aViewGroup.addView(view);

        System.out.println("创建View树");

        System.out.println("模拟从Activity开始的点击事件");

        Activity activity = new Activity();
        activity.mWindow.mDecor.addView(rootViewGroup);

        // 模拟点击事件
        activity.dispatchTouchEvent(new MotionEvent());

    }


    /**
     * Activity的dispatchTouchEvent()方法，是事件向下分发的起点，可以覆写该方法，直接返回true或false将该事件拦截，扼杀在摇篮里
     */
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // 内部会继续调用子View的dispatchTouchEvent()方法
        // 1. 如果该方法返回true，表示子View将事件分发了下去
        if (mWindow.superDispatchTouchEvent(ev)) {
            return true;
        }

        // 2. 如果该方法返回false，表示子View不想分发事件，那么事件会进入到Activity自己的onTouchEvent()方法中
        return onTouchEvent(ev);
    }

    public boolean onTouchEvent(MotionEvent event) {
//        if (mWindow.shouldCloseOnTouch(this, event)) {
//            finish();
//            return true;
//        }

        return false;
    }

}
