package pfq.demo.designpattern.chainofresponsibilitypattern.okhttp;

public class AInterceptor implements Interceptor {
    private String mName;

    AInterceptor(String name) {
        mName = name;
    }

    @Override
    public Response intercept(Chain chain) {
        System.out.println("正在执行[" + mName + "]拦截器的拦截方法");
        return chain.proceed(chain.request());
    }
}
