package pfq.demo.designpattern.chainofresponsibilitypattern.activity;

/**
 * Android源码中的DecorView是继承于FrameLayout
 */
public class DecorView extends ViewGroup {

    DecorView(String name) {
        super(name);
    }

    public boolean superDispatchTouchEvent(MotionEvent event) {
        // DecorView是继承于FrameLayout，所以调用的是ViewGroup的dispatchTouchEvent()方法
        return super.dispatchTouchEvent(event);
    }
}
