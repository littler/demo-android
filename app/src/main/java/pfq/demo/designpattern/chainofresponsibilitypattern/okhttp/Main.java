package pfq.demo.designpattern.chainofresponsibilitypattern.okhttp;

import java.util.ArrayList;
import java.util.List;

/**
 * OkHttp中拦截器的实现也是一种责任链模式，但和activity的事件分发机制的责任链模式还不一样，其各个责任实体对象相互直接并不会依赖，例如：A->B->C->D
 * 而是通过创建一个Chain对象来实现责任实体对象间的连接，很形象，内部可以获取当前位置的责任实体对象，调用责任对象的方法，并传入可以获取下一个责任对象的Chain对象，所以此处专门用代码实现一遍
 */
public class Main {
    public static void main(String[] args) {

        // 批量创建拦截器
        List<Interceptor> interceptors = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            interceptors.add(new AInterceptor("" + i));
        }


        Request request = new Request();
        Interceptor.Chain chain = new RealInterceptorChain(request, 0, interceptors);
        chain.proceed(request);

    }


}
