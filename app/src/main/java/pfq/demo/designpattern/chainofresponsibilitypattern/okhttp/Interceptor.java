package pfq.demo.designpattern.chainofresponsibilitypattern.okhttp;

// 拦截器
public interface Interceptor {
    Response intercept(Chain chain);

    // 链，责任链模式中的链，用来将各个责任实体对象串起来的链，在OkHttp中就是将各个拦截器串起来的链，可以说是很形象了
    interface Chain {
        Response proceed(Request request);

        Request request();
    }
}
