package pfq.demo.designpattern.chainofresponsibilitypattern.activity;

public class PhoneWindow {

    DecorView mDecor = new DecorView("DecorView");

    public boolean superDispatchTouchEvent(MotionEvent event) {
        // Window的内部又会调用DecorView的方法
        return mDecor.superDispatchTouchEvent(event);
    }
}
