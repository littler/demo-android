package pfq.demo.designpattern.proxy;

/**
 * 卖票接口
 * Created by pangfuqiang on 2018/2/3.
 */

public interface ISell {
    void sellTicket();

    void sellTeamTicket();
}
