package pfq.demo.designpattern.proxy;

import java.lang.reflect.Proxy;

/**
 * Created by pangfuqiang on 2018/2/3.
 */

public class Main {
    public static void main(String[] args) {

        System.out.println("=====静态代理=====");

        // 先创建一个火车站
        TrainStation trainStation = new TrainStation();
        // 再创建一个代售点
        TicketAgency ticketAgency = new TicketAgency(trainStation);
        // 开始售票
        ticketAgency.sellTicket();

        System.out.println("=====动态代理=====");

        // 先创建一个火车站
        TrainStation trainStation1 = new TrainStation();
        // 通过动态代理来生成一个代售点
        ISell sell = (ISell) Proxy.newProxyInstance(ISell.class.getClassLoader(), new Class[]{ISell.class}, new DynamicProxy(trainStation1));
        sell.sellTicket();
        sell.sellTeamTicket();

    }
}
