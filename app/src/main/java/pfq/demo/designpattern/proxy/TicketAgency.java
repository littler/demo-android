package pfq.demo.designpattern.proxy;

/**
 * 车票代售点
 * Created by pangfuqiang on 2018/2/3.
 */

public class TicketAgency implements ISell {
    ISell iSell;

    public TicketAgency(ISell sell) {
        this.iSell = sell;
    }

    @Override
    public void sellTicket() {

        System.out.println("before");

        if (iSell != null) {
            iSell.sellTicket();
        }

        System.out.println("after");
    }

    @Override
    public void sellTeamTicket() {

    }
}
