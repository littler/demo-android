package pfq.demo.designpattern.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Created by pangfuqiang on 2018/2/3.
 */

public class DynamicProxy implements InvocationHandler {

    Object obj;

    public DynamicProxy(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {

        System.out.println("before");
        method.invoke(obj, (Object) null);
        System.out.println("after");

        return null;
    }
}
