package pfq.demo.designpattern.proxy;

/**
 * 火车站
 * Created by pangfuqiang on 2018/2/3.
 */

public class TrainStation implements ISell {
    @Override
    public void sellTicket() {
        System.out.println("火车站售票");
    }

    @Override
    public void sellTeamTicket() {
        System.out.println("火车站售团体票");
    }
}
