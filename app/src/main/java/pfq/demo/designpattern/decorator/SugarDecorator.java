package pfq.demo.designpattern.decorator;

public class SugarDecorator extends Decorator {
    public SugarDecorator(Coffee coffee) {
        super(coffee);
    }

    @Override
    public double getPrice() {
        return mCoffee.getPrice() + 2;
    }

    @Override
    public String getDesc() {
        return "加糖后价格：" + getPrice() + "元";
    }

}
