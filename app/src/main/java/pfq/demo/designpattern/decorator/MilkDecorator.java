package pfq.demo.designpattern.decorator;

public class MilkDecorator extends Decorator {
    public MilkDecorator(Coffee coffee) {
        super(coffee);
    }

    @Override
    public double getPrice() {
        return mCoffee.getPrice() + 5;
    }

    @Override
    public String getDesc() {
        return "加牛奶后价格：" + getPrice() + "元";
    }

}