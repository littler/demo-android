package pfq.demo.designpattern.decorator;

/**
 * 原味咖啡
 */
public class SimpleCoffee implements Coffee {
    @Override
    public double getPrice() {
        return 5;
    }

    @Override
    public String getDesc() {
        return "原味咖啡价格：" + getPrice() + "元";
    }
}
