package pfq.demo.designpattern.decorator;

/**
 * 装饰者模式：（包装类）
 * <p>
 * 1. 动态地将功能附加到主对象上，起修饰的作用，有点像动态代理，在方法执行的前后添加代码
 * 2. 扩展了主对象的功能，装饰者模式提供了有别于继承的另一种选择（PS：类似于对象组合的方式）
 */
public class Main {
    public static void main(String[] args) {
        // 先创建一杯原味咖啡，售价5元
        Coffee coffee = new SimpleCoffee();
        // 原味
        System.out.println(coffee.getDesc());

        // 加糖
        coffee = new SugarDecorator(coffee);
        System.out.println(coffee.getDesc());

        // 加奶
        coffee = new MilkDecorator(coffee);
        System.out.println(coffee.getDesc());

    }
}
