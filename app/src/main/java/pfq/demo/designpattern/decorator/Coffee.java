package pfq.demo.designpattern.decorator;

/**
 * 核心接口
 */
public interface Coffee {

    /**
     * 返回价格
     */
    double getPrice();

    /**
     * 返回描述
     */
    String getDesc();

}
