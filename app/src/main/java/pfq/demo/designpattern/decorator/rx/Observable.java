package pfq.demo.designpattern.decorator.rx;

/**
 * 被观察者
 */
public abstract class Observable implements ObservableSource {

    @Override
    public void subscribe(Observer observer) {
        System.out.println("Observable: subscribe()");
    }

    /**
     * 创建Observable对象
     */
    public static Observable create() {
        return new Observable() {
        };
    }

    public Observable subscribeOn(Scheduler scheduler) {
        // 用装饰者修饰一下
        return new ObservableSubscribeOn(this, scheduler);
    }

}
