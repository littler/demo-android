package pfq.demo.designpattern.decorator.rx;

/**
 * 发射源
 */
public interface ObservableSource {
    void subscribe(Observer observer);
}
