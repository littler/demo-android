package pfq.demo.designpattern.decorator.rx;

/**
 * 演示了RxJava中的装饰者模式
 */
public class Main {
    public static void main(String[] args) {
        Observable observable = Observable.create();

        observable.subscribeOn(Scheduler.IO).subscribe(Observer.create());


    }
}
