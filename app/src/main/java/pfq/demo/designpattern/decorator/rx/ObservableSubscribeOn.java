package pfq.demo.designpattern.decorator.rx;


public class ObservableSubscribeOn extends Observable {


    public ObservableSubscribeOn(Observable observable, Scheduler scheduler) {

    }

    @Override
    public void subscribe(Observer observer) {
        System.out.println("ObservableSubscribeOn: subscribe()");
    }
}
