package pfq.demo.designpattern.decorator;

/**
 * 1. 和被装饰的对象拥有相同的父类或接口
 * 2. 通过构造函数将被装饰的对象传进来加以修饰
 */
public abstract class Decorator implements Coffee {

    protected Coffee mCoffee;

    public Decorator(Coffee coffee) {
        mCoffee = coffee;
    }

}
