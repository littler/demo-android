package pfq.demo.designpattern.simplefatory;

/**
 * 简单工厂模式是工厂方法模式的一种简化版，其并不属于23种设计模式
 * 与工厂方法模式的区别在于工厂类：
 * 1. 工厂类没有父类
 * 2. 直接使用静态方法创建产品
 * Created by pangfuqiang on 2017/6/20.
 */

public class ConcreteCreator {
    public static <T extends Product> T createProduct(Class<T> c) {

        Product product = null;

        try {
            product = (Product) Class.forName(c.getName()).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return (T) product;
    }
}
