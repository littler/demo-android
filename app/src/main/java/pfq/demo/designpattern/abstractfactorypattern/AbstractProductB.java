package pfq.demo.designpattern.abstractfactorypattern;

/**
 * Created by pangfuqiang on 2017/6/20.
 */

public abstract class AbstractProductB {
    // 每个产品共有的方法
    public void shareMethod() {

    }

    // 每个产品相同的方法，但实现不同
    public abstract void doSomething();
}
