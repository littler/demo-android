package pfq.demo.designpattern.abstractfactorypattern;

/**
 * Created by pangfuqiang on 2017/6/20.
 */

public class Creator2 extends AbstractCreator {
    @Override
    public AbstractProductA createProductA() {
        return new ProductA2();
    }

    @Override
    public AbstractProductB createProductB() {
        return new ProductB2();
    }
}
