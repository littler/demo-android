package pfq.demo.designpattern.abstractfactorypattern;

/**
 * Created by pangfuqiang on 2017/6/20.
 */

public class Creator1 extends AbstractCreator {
    @Override
    public AbstractProductA createProductA() {
        return new ProductA1();
    }

    @Override
    public AbstractProductB createProductB() {
        return new ProductB1();
    }
}
