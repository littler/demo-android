package pfq.demo.designpattern.abstractfactorypattern;

/**
 * 有N个产品，在抽象工厂类中就应该有N个创建方法
 * Created by pangfuqiang on 2017/6/20.
 */

public abstract class AbstractCreator {

    public abstract AbstractProductA createProductA();

    public abstract AbstractProductB createProductB();
}
