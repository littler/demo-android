package pfq.demo.designpattern.iterator;

import java.util.Vector;

/**
 * 具体迭代器
 */
public class ConcreteIterator implements Iterator {
    // 类似于ArrayList，只是Vector是线程安全的
    private Vector vector = new Vector();

    // 定义当前游标
    public int cursor = 0;

    public ConcreteIterator(Vector vector) {
        this.vector = vector;
    }


    @Override
    public Object next() {
        if (this.hasNext()) {
            return this.vector.get(this.cursor++);
        }

        return null;
    }

    @Override
    public boolean hasNext() {
        return !(this.cursor == this.vector.size());
    }

    @Override
    public boolean remove() {
        this.vector.remove(this.cursor);
        return true;
    }
}
