package pfq.demo.designpattern.iterator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

/**
 * 为什么会有迭代器模式？
 * <p>
 * 迭代器，顾名思义，就是用来遍历集合的，但为什么会有专门的对象，也就是迭代器来处理集合的遍历问题呢？
 * 其实迭代器的出现主要是用来解决各种集合（list、set、queue、map的key和value）因为各自内部数据结构的不同导致遍历方式不一致的问题
 * 而迭代器为他们提供了统一的接口，统一的方式去遍历
 * <p>
 * 所以，迭代器就是用来遍历集合并屏蔽集合内部结构的一个对象
 */
public class Client {

    public static void main(String[] args) {

        // 声明出容器
        Aggregate agg = new ConcreteAggregate();

        // 产生对象数据放进去
        agg.add("abc0");
        agg.add("abc1");
        agg.add("abc2");

        // 遍历一下
        Iterator iterator = agg.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        // 遍历List
        iteratorData(getList().iterator());

        // 遍历Set
        iteratorData(getSet().iterator());

        // 遍历Queue
        iteratorData(getQueue().iterator());

        // 遍历Map
        iteratorData(getMap().keySet().iterator());
        iteratorData(getMap().values().iterator());
    }

    public static Map<String, String> getMap() {

        Map<String, String> stringStringMap = new HashMap<>();
        for (int i = 0; i < 5; i++) {
            stringStringMap.put("key " + i, "value " + i);
        }
        System.out.println("=====我是Map=====");
        return stringStringMap;
    }

    public static Queue<String> getQueue() {
        Queue<String> queue = new PriorityQueue<>();
        for (int i = 0; i < 5; i++) {
            queue.add("item " + i);
        }
        System.out.println("=====我是Queue=====");
        return queue;
    }


    public static List<String> getList() {
        List<String> data = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            data.add("item " + i);
        }
        System.out.println("=====我是List=====");
        return data;
    }

    public static Set<String> getSet() {
        Set<String> set = new HashSet<>();
        for (int i = 0; i < 5; i++) {
            set.add("item " + i);
        }
        System.out.println("=====我是Set=====");
        return set;
    }

    public static void iteratorData(java.util.Iterator iterator) {
        if (iterator == null) {
            return;
        }
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

}

