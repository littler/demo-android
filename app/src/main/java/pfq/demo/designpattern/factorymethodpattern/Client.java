package pfq.demo.designpattern.factorymethodpattern;

/**
 * Created by pangfuqiang on 2017/6/20.
 */

public class Client {

    public static void main(String[] args) {

        Creator creator = new ConcreteCreator();

        Product product = creator.createProduct(ConcreteProduct1.class);

    }

}
