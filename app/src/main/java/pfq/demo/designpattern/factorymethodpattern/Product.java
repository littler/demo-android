package pfq.demo.designpattern.factorymethodpattern;

/**
 * 抽象产品类
 * Created by pangfuqiang on 2017/6/20.
 */

public abstract class Product {

    // 产品类的公共方法
    public void method1() {
        // 业务逻辑处理
    }

    public abstract void method2();


}
