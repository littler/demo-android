package pfq.demo.designpattern.factorymethodpattern;

/**
 * Created by pangfuqiang on 2017/6/20.
 */

public abstract class Creator {

    public abstract <T extends Product> T createProduct(Class<T> c);

}
