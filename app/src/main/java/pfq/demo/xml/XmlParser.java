package pfq.demo.xml;

import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;

import java.io.InputStream;

/**
 * 从xml的pull解析后的打印信息来看，布局文件的解析是深度优先，其实pull解析就是因为读取一行，解析一行，所以才是深度优先
 */
public class XmlParser {

    public static void parse(InputStream is) throws Exception {

        if (is == null) {
            Log.d("pangfq", "InputStream instance is null");
            return;
        }

        XmlPullParser xmlPullParser = Xml.newPullParser();
        xmlPullParser.setInput(is, "UTF-8");

        int eventType = xmlPullParser.next();
        do {
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    Log.d("pangfq", "<" + xmlPullParser.getName() + ">");
                    break;
                case XmlPullParser.END_TAG:
                    Log.d("pangfq", "</" + xmlPullParser.getName() + ">");
                    break;
            }

            eventType = xmlPullParser.next();
        } while (eventType != XmlPullParser.END_DOCUMENT);

    }

}
