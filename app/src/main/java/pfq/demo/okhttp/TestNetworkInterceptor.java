package pfq.demo.okhttp;

import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public class TestNetworkInterceptor implements Interceptor {
    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {

        Log.d("pangfq", "TestNetworkInterceptor");

        return chain.proceed(chain.request());
    }
}
