package pfq.demo.architecture.mvvm;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import pfq.demo.R;
import pfq.demo.databinding.ActivityMvvmBinding;

public class MvvmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMvvmBinding activityMvvmBinding = DataBindingUtil.setContentView(this, R.layout.activity_mvvm);

        final User user = new User();
        user.setName("张三");

        activityMvvmBinding.setUser(user);

        findViewById(R.id.activity_mvvm_name_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // 通过更新data就可以自动更新UI
                user.setName("李四");
            }
        });
    }
}
