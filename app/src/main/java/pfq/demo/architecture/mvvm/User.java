package pfq.demo.architecture.mvvm;

import androidx.databinding.BaseObservable;

public class User extends BaseObservable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;

        notifyChange();
    }

}
