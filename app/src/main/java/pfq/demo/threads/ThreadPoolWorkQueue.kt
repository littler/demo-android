package pfq.demo.threads

import java.util.concurrent.LinkedBlockingDeque
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

class Task(what: Int) : Runnable {

    private var what: Int = 0

    init {
        this.what = what
    }

    override fun run() {
        // 打印what参数
        println("what: $what")
        // 休眠2s
        Thread.sleep(10000)
    }

}

/**
 * 线程池的工作队列示例
 */
fun main() {

    // 创建一个核心线程数为1，允许最大线程数为2，工作队列是FIFO并且大小为2的队列的线程池
    val fixedES = ThreadPoolExecutor(1, 2,
            0L, TimeUnit.MILLISECONDS,
            LinkedBlockingDeque<Runnable>(2))
    // 依次创建5个任务交给线程池去处理
    for (i in 1..5) {
        // 没有返回值，直接execute()方法就可以了
        fixedES.execute(Task(i))
    }

    // 执行的流程是：
    // 1. 线程池因为有一个核心线程，所以会先执行第一个任务[结果为：what：1]
    // 2. 接着进入的第二、第三个任务因为线程池的核心线程已满，所以只能先进入到队列中进行排队[结果为：等待]
    // 3. 然后第四个任务进入到线程池，因为队列已满，并且没有达到最大线程数，所以线程池另起一个非核心线程立刻执行第四个任务[结果为：what：4]
    // 4. 最后第五个任务进入后，因为已经达到线程池最大的线程数，所以直接执行拒绝策略，默认是AbortPolicy抛出异常[结果为：异常]

}