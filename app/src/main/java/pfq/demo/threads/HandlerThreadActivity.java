package pfq.demo.threads;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import pfq.demo.R;

/**
 * HandlerThread: 看名字是一个带handler的thread，但其实是带looper的thread，我非常好奇google为啥不起名为LooperThread
 * 当然非要叫HandlerThread也没问题，因为Looper少了Handler也不能用，同样Handler少了Looper也不能用，因为Android的Handler机制
 * 其实是Handler+Looper+MessageQueue一块儿说的，少了哪一个都不行。
 * <p>
 * 所以HandlerThread其实就是一个带了消息队列的线程，可以通过Handler来不断发送任务到HandlerThread中依次执行，最后在不用的时候将其退出。
 * <p>
 * 比普通的Thread稍微强大了些而已，叫作【消息队列线程】会更贴切。
 */
public class HandlerThreadActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView mLabelTextView;
    // 子线程Handler
    private Handler mHandlerThreadHandler;
    private HandlerThread mHandlerThread;

    private static final String[] sTask = {"http://aaa.com", "http://bbb.com", "http://ccc.com", "http://ddd.com"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handler_thread);

        findViewById(R.id.activity_handler_thread_start_btn).setOnClickListener(this);
        findViewById(R.id.activity_handler_thread_stop_btn).setOnClickListener(this);

        mLabelTextView = findViewById(R.id.activity_handler_thread_label_tv);

        // 创建一个带有消息队列的线程
        mHandlerThread = new HandlerThread("handler-thread");
        mHandlerThread.start();
        // 创建一个往消息队列线程里发送任务的Handler
        mHandlerThreadHandler = new Handler(mHandlerThread.getLooper());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_handler_thread_start_btn: {

                mLabelTextView.setText("开始");

                for (int i = 0; i < sTask.length; i++) {
                    final String task = sTask[i];
                    mHandlerThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 模拟网络耗时
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            // 模拟返回数据
                            final String result = task;

                            // 调用系统主线程的handler将结果更新到UI
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mLabelTextView.setText(mLabelTextView.getText().toString() + "\n" + result);
                                }
                            });

                        }
                    });
                }

                break;
            }

            case R.id.activity_handler_thread_stop_btn: {

                // 也可以直接调用HandlerThread的quit()方法直接退出消息队列线程
                mHandlerThreadHandler.getLooper().quitSafely();
                mLabelTextView.setText(mLabelTextView.getText().toString() + "\n" + "停止");
                break;
            }
        }
    }
}
