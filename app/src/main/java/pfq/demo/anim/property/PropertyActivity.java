package pfq.demo.anim.property;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import pfq.demo.R;

public class PropertyActivity extends Activity {

    private ImageView mImageView1, mImageView2;
    private ImageView mImageViewObj1, mImageViewObj2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anim_property);

        // ViewPropertyAnimator
        mImageView1 = findViewById(R.id.imageview1);
        mImageView2 = findViewById(R.id.imageview2);

        mImageView1.setX(50);
        mImageView2.setX(50);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // 平移到500 = 500
                mImageView1.animate().translationX(500);
                // 再平移500 = 50 + 500 = 550，表示增量
                mImageView2.animate().translationXBy(500);

            }
        }, 1000);


        // ObjectAnimator
        mImageViewObj1 = findViewById(R.id.imageviewObj1);
        mImageViewObj2 = findViewById(R.id.imageviewObj2);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mImageViewObj1.animate().translationX(500).setInterpolator(new LinearInterpolator());

                // ObjectAnimator的使用
                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mImageViewObj2, "translationX", 500);
                objectAnimator.setInterpolator(new AnticipateOvershootInterpolator());
                objectAnimator.start();

            }
        }, 1000);

    }

}
