package pfq.demo.handler

import android.os.Bundle
import android.os.Message
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_handler.*
import pfq.demo.R

class HandlerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_handler)

        send2SubThreadBtn.setOnClickListener {


            // 在主线程创建消息
            val msg = Message.obtain()
            msg.what = 22222

            // 开启子线程
            val subThread = SubThread()
            subThread.start()

            Thread.sleep(2000)

            // 向子线程发送消息
            Log.d("pangfq", "开始发送消息：" + msg.what + ", 当前线程：" + Thread.currentThread().name)
            subThread.handler?.sendMessage(msg)
        }
    }
}
