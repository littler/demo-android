package pfq.demo.handler

import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log

class SubThread : Thread() {

    var handler: Handler? = null

    override fun run() {
        super.run()

        Log.d("pangfq", "当前线程：" + Thread.currentThread().name)

        // 为该线程绑定一个looper对象
        Looper.prepare()

        handler = object : Handler() {
            override fun handleMessage(msg: Message) {
                super.handleMessage(msg)

                Log.d("pangfq", "收到消息了：" + msg.what + ", 当前线程：" + Thread.currentThread().name)
            }
        }

        // 开启轮询
        Looper.loop()

        Thread.sleep(5000)

    }
}