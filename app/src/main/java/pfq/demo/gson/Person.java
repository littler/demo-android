package pfq.demo.gson;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Since;
import com.google.gson.annotations.Until;

import java.io.Serializable;

/**
 * Created by pangfuqiang on 2018/2/24.
 */

@JsonAdapter(value = BooleanJsonAdapter.class)
public class Person implements Serializable {

    @Since(1.0)
    private int age;
    @Since(2.0)
    private String name;

    @Until(2.0)
    private int height;

    private boolean isMale;

    @SerializedName(value = "avatar_url", alternate = {"header_url", "header_icon"})
    private String avatarUrl;

    public boolean isMale() {
        return isMale;
    }

    public void setMale(boolean male) {
        isMale = male;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
