package pfq.demo.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by pangfuqiang on 2018/2/24.
 */

class Main {
    public static void main(String[] args) {

        Person person = new Person();
        person.setAge(27);
        person.setName("pangfuqiang");
        person.setHeight(172);
        person.setMale(true);
        person.setAvatarUrl("http://www.baidu.com/xxx.png");

        String json = "{\"age\":27,\"name\":\"pangfuqiang\",\"isMale\":1,\"avatar_url\":\"http://www.baidu.com/xxx.png\"}";

//        Gson gson1 = new GsonBuilder().setVersion(1.0).create();
//        System.out.println(gson1.toJson(person));

        Gson gson2 = new GsonBuilder().setVersion(2.0).create();

        System.out.println(gson2.toJson(gson2.fromJson(json, Person.class)));

    }

}
