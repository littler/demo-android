package pfq.demo.view;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import pfq.demo.R;

/**
 * 演示了ViewGroup如果拦截了DOWN事件，则MOVE和UP事件会不会向下传递
 * 答：不会向下传递了。
 */
public class DispatchEventActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispatch_event);

        View btnA = findViewById(R.id.activity_action_cancel_btn_a);
        btnA.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_action_cancel_btn_a: {
                Log.d("pangfq", "A Button is clicked");
                Toast.makeText(DispatchEventActivity.this, "A", Toast.LENGTH_SHORT).show();
                break;
            }

            case R.id.activity_action_cancel_btn_b: {
                Toast.makeText(DispatchEventActivity.this, "B", Toast.LENGTH_SHORT).show();
                break;
            }
        }
    }
}
