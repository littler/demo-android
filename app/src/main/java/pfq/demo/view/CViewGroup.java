package pfq.demo.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.FrameLayout;

/**
 * TODO: document your custom view class.
 */
public class CViewGroup extends FrameLayout {

    public CViewGroup(Context context) {
        super(context);
        init(null, 0);
    }

    public CViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public CViewGroup(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        Log.d("ViewRefresh", "CViewGroup-onMeasure()");
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        Log.d("ViewRefresh", "CViewGroup-onLayout()");
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.d("ViewRefresh", "CViewGroup-onDraw()");
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        // 当触摸点到达自身顶部的时候，进行事件的拦截，但不复写onTouchEvent()方法，相当于直接消费掉事件不做任何处理
        if (ev.getY() <= 0) {
            return true;
        }

        return super.onInterceptTouchEvent(ev);
    }

}
