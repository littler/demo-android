package pfq.demo.view;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import pfq.demo.R;

/**
 * 演示了ACTION_CANCEL事件的触发时机：
 * 当子控件接收到DOWN事件后，父控件拦截了MOVE或者UP事件，则子控件就会收到CANCEL事件
 * <p>
 * 并不是我刚开始想象中的：当按下按钮并滑出按钮范围后该按钮会收到CANCEL事件，其实并不会，而是MOVE和UP事件正常收到
 */
public class ActionCancleActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action_cancle);

        View btnA = findViewById(R.id.activity_action_cancel_btn_a);
        btnA.setOnClickListener(this);
        btnA.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("pangfq", "A Button onTouch: " + event.getAction());
                return false;
            }
        });

        findViewById(R.id.activity_action_cancel_btn_b).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_action_cancel_btn_a: {
                Log.d("pangfq", "A Button is clicked");
                Toast.makeText(ActionCancleActivity.this, "A", Toast.LENGTH_SHORT).show();
                break;
            }

            case R.id.activity_action_cancel_btn_b: {
                Toast.makeText(ActionCancleActivity.this, "B", Toast.LENGTH_SHORT).show();
                break;
            }
        }
    }
}
