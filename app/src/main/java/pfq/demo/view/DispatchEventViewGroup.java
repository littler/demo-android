package pfq.demo.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

/**
 * TODO: document your custom view class.
 */
public class DispatchEventViewGroup extends LinearLayout {

    public DispatchEventViewGroup(Context context) {
        super(context);
        init(null, 0);
    }

    public DispatchEventViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public DispatchEventViewGroup(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        // ViewGroup只拦截DOWN事件
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            return true;
        }

        return super.onInterceptTouchEvent(ev);
    }
}
