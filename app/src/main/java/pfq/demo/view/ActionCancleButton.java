package pfq.demo.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

/**
 * TODO: document your custom view class.
 */
public class ActionCancleButton extends androidx.appcompat.widget.AppCompatButton {


    public ActionCancleButton(Context context) {
        super(context);
    }

    public ActionCancleButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        // 有面试官问onTouchEvent()方法和onTouch()方法的关系？
        // 其实两方法都会在dispatchTouchEvent()方法中被调用，是并列的关系
        // 如果此处覆写了dispatchTouchEvent()方法的话，会导致自身的onTouch()和onTouchEvent()不被调用
        return super.dispatchTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_CANCEL) {
            Log.d("pangfq", "ActionCancleButton-MotionEvent-ACTION_CANCEL");
        } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
            Log.d("pangfq", "ActionCancleButton-MotionEvent-ACTION_DOWN");
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            Log.d("pangfq", "ActionCancleButton-MotionEvent-ACTION_MOVE");
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            Log.d("pangfq", "ActionCancleButton-MotionEvent-ACTION_UP");
        } else {
            Log.d("pangfq", "ActionCancleButton-MotionEvent-" + event.getAction());
        }


        // 有面试官问到onTouchEvent()方法和点击事件onClick()方法的关系？
        // 其实点击事件的回调方法onClick()就是在onTouchEvent()中被回调的，所以你要是覆写了一个控件的onTouchEvent()方法
        // 直接return true或false; 而不调用super.onTouchEvent()，也就是父类的onTouchEvent()方法的话，会导致自身点击事件失效
        return super.onTouchEvent(event);
    }
}
