package pfq.demo.view

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import pfq.demo.R

class CustomViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d("pangfq", "Activity-onCreate-before setContentView()---" + Thread.currentThread().name);

        setContentView(R.layout.activity_custom_view)

        Log.d("pangfq", "Activity-onCreate-after setContentView()---" + Thread.currentThread().name);
    }

    override fun onStart() {
        super.onStart()

        Log.d("pangfq", "Activity-onStart");
    }

    override fun onResume() {
        super.onResume()

        Log.d("pangfq", "Activity-onResume");
    }

}
