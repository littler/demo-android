package pfq.demo.view

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log

/**
 * TODO: document your custom view class.
 */
class CustomTextView : androidx.appcompat.widget.AppCompatTextView {


    constructor(context: Context) : super(context) {
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        Log.d("pangfq", "TextView-onMeasure")
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)

        Log.d("pangfq", "TextView-onLayout")
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        Log.d("pangfq", "TextView-onDraw")

    }
}
