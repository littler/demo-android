package pfq.demo.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import pfq.demo.R

/**
 * 1. FrameLayout的layout_gravity属性的用法，并不是FrameLayout只是简单的帧布局，一层覆盖在另一层上面，子控件还可以借助layout_gravity来改变自己在FrameLayout的位置
 * 2. layout_gravity: 是子控件设置的属性，用来表示自身在父控件中的位置。需要注意的是：该属性的值可以设置多个，比如左上 left|top，右下 right|bottom，并不是只能设置一个值
 */
class FrameLayoutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_frame_layout)
    }
}
