package pfq.demo.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import pfq.demo.R;

public class RequestLayoutInvalidateActivity extends AppCompatActivity implements View.OnClickListener {

    private Button requestLayoutBtn;
    private Button invalidateBtn;
    private Button invalidatePostBtn;

    private View aView;
    private View bViewGroup;
    private View cViewGroup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_layout_invalidate);

        requestLayoutBtn = findViewById(R.id.activity_request_layout_invalidate_request_layout_btn);
        requestLayoutBtn.setOnClickListener(this);

        invalidateBtn = findViewById(R.id.activity_request_layout_invalidate_btn);
        invalidateBtn.setOnClickListener(this);

        invalidatePostBtn = findViewById(R.id.activity_request_layout_invalidate_post_btn);
        invalidatePostBtn.setOnClickListener(this);

        aView = findViewById(R.id.activity_request_layout_invalidate_aview);
        bViewGroup = findViewById(R.id.activity_request_layout_invalidate_bview);
        cViewGroup = findViewById(R.id.activity_request_layout_invalidate_cview);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_request_layout_invalidate_request_layout_btn:
                // 调用View树，重新走三大流程
                aView.requestLayout();
                break;
            case R.id.activity_request_layout_invalidate_btn:
                // 只会调用AView的onDraw()
                aView.invalidate();
                break;
            case R.id.activity_request_layout_invalidate_post_btn:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // 可以在子线调用postInvalidate()刷新View
                        aView.postInvalidate();
                    }
                }).start();
                break;
        }
    }
}
