package pfq.demo.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import pfq.demo.R

/**
 * 这是一道"夜神模拟器"公司的面试题，他们家的面试题不多，但都比较贴近实战，还是可以考察出一个人实战水平的，所以记录下来。
 *
 * 这道题是：在Activity中有三个控件，A B C，A是B的子控件，B是C的子控件，要求拖动A控件到达B控件顶部时，B控件跟随一起滑动，接着到达C控件顶部停止
 *
 * 这道题考察的是事件分发机制+View坐标系+触摸事件MotionEvent，解题思路是：
 * 1. 事件是从ViewGroup传递到View的，所以对于C来讲，要覆写onInterceptTouchEvent()，在该方法中判断motionEvent.getY()==0则return true;这样可以拦截掉事件，因为是到达C控件顶部停止，所以拦截掉即可，不用覆写onTouchEvent()方法
 * 2. 对于B来讲，跟C一样同样覆写onInterceptTouchEvent()方法，但因为B要随A一起滑动，所以还要再覆写onTouchEvent()方法，实时更新B的setY()即可
 * 3. 对于C来讲，因为是View，所以没有onInterceptTouchEvent()方法，只要覆写onTouchEvent()方法，实时更新A的setY()方法即可。
 *
 * 另外需要掌握：
 * 1. View的坐标系，这个需要跟屏幕的坐标系区分开，屏幕的坐标系是绝对坐标系，原点在屏幕左上角，向右是x轴正方向，向下是y轴正方向
 *    而View的坐标系是相对坐标系，是相对其父控件的坐标系，比如A是B的子控件，那A的x、y坐标就是基于父控件B计算的，对应api是setX()、setY()
 * 2. MotionEvent是触摸事件，有两种方法获取事件触摸点的坐标，event.getX()/event.getRawX()，同样是有两种坐标系，event.getX()得到的是相对View自身的额坐标系的x坐标，而event.getRawX()是绝对坐标，就是基于屏幕坐标系的x坐标
 *
 * 总结：了解了上述知识点后，就可以轻松完成此题目
 */
class ViewEventDispatchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_event_dispatch)
    }
}
