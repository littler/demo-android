package pfq.demo.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.constraintlayout.widget.ConstraintLayout;

/**
 * TODO: document your custom view class.
 */
public class ActionCancleViewGroup extends ConstraintLayout {

    public ActionCancleViewGroup(Context context) {
        super(context);
        init(null, 0);
    }

    public ActionCancleViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public ActionCancleViewGroup(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        // 父控件拦截掉move或者up事件都会让子控件收到cancel事件，并且只要拦截掉一次，之后的move、up事件是不会再传递到子控件的，这里只简单示例。
        if (ev.getAction() == MotionEvent.ACTION_MOVE) {
            return true;
        }

        return super.onInterceptTouchEvent(ev);
    }
}
