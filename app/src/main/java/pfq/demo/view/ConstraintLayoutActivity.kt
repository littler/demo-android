package pfq.demo.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import pfq.demo.R

/**
 * 约束布局
 *
 * 可以代替RelativeLayout，功能还是挺强大的，支持：
 * 1. 相对定位
 * 2. 角度定位
 * 3. 链、比重、比例 等等
 */
class ConstraintLayoutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_constraint_layout)
    }
}
