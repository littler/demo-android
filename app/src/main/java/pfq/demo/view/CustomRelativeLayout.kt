package pfq.demo.view

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout

/**
 * TODO: document your custom view class.
 */
class CustomRelativeLayout : RelativeLayout {


    constructor(context: Context) : super(context) {
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        Log.d("pangfq", "CustomRelativeLayout-onMeasure")
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)

        Log.d("pangfq", "CustomRelativeLayout-onLayout")
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        Log.d("pangfq", "CustomRelativeLayout-onDraw")

    }

    override fun addView(child: View?, params: ViewGroup.LayoutParams?) {
        super.addView(child, params)
        Log.d("pangfq", "CustomRelativeLayout-addView")
    }
}
