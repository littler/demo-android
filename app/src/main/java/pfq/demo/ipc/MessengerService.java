package pfq.demo.ipc;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.Nullable;

public class MessengerService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    private final static Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            Log.d("ipc-messenger", "收到消息: " + msg.what);

            // 获取Messenger
            Messenger messenger = msg.replyTo;
            // 创建消息
            Message msg_reply = Message.obtain();
            msg_reply.what = 5120;
            try {
                messenger.send(msg_reply);
            } catch (RemoteException e) {
                e.printStackTrace();
            }


        }
    };

    private final static Messenger mMessenger = new Messenger(mHandler);

}
