package pfq.demo.ipc;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import pfq.demo.IShop;
import pfq.demo.R;


public class IPCActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ipc);

        findViewById(R.id.activity_enter_ipc_messenger_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IPCActivity.this, MessengerService.class);
                bindService(intent, new MessengerServiceConnection(), BIND_AUTO_CREATE);
            }
        });

        findViewById(R.id.activity_enter_ipc_aidl_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IPCActivity.this, AIDLService.class);
                bindService(intent, new AIDLServiceConnection(), BIND_AUTO_CREATE);
            }
        });

    }

    private class AIDLServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // 获取到service传过来的对象
            IShop iShop = IShop.Stub.asInterface(service);
            try {
                Product product = new Product();
                product.price = 100;
                product.name = "一颗柠檬茶";
                iShop.setProduct(product);

                Product buyResult = iShop.buy();

                Log.d("ipc-aidl", "Product name：" + buyResult.name);

            } catch (RemoteException e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }

        @Override
        public void onBindingDied(ComponentName name) {

        }
    }

    private class MessengerServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("ipc-messenger", "onServiceConnected");
            // 通过Binder创建Messenger
            Messenger messenger = new Messenger(service);
            Message msg = Message.obtain();
            msg.what = 520;
            msg.replyTo = mReplayMessenger;
            try {
                messenger.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("ipc-messenger", "onServiceDisconnected");
        }

        @Override
        public void onBindingDied(ComponentName name) {
            Log.d("ipc-messenger", "onBindingDied");
        }
    }

    private final static Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.d("ipc-messenger", "收到你的回复: " + msg.what);

        }
    };

    private final static Messenger mReplayMessenger = new Messenger(mHandler);
}
