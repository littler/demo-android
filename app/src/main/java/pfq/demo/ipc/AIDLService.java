package pfq.demo.ipc;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.Nullable;
import pfq.demo.IShop;

public class AIDLService extends Service {
    private Product mProduct;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private Binder mBinder = new IShop.Stub() {

        @Override
        public Product buy() throws RemoteException {
            mProduct.name = "奈雪的茶";
            return mProduct;
        }

        @Override
        public void setProduct(Product product) throws RemoteException {
            mProduct = product;
            Log.d("ipc-aidl", "客户端传来的Product：" + product.name);
        }

    };

}
