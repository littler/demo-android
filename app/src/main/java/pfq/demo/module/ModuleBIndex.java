package pfq.demo.module;

import com.yd.demo.arouter.IIndex;
import com.yd.demo.module_b.BActivity;


public class ModuleBIndex implements IIndex {

    @Override
    public String getPath() {
        return "/module/b";
    }

    @Override
    public Class getClaz() {
        return BActivity.class;
    }
}
