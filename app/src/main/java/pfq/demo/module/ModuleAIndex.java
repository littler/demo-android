package pfq.demo.module;

import com.yd.demo.arouter.IIndex;
import com.yd.demo.module_a.AActivity;

public class ModuleAIndex implements IIndex {

    @Override
    public String getPath() {
        return "/module/a";
    }

    @Override
    public Class getClaz() {
        return AActivity.class;
    }
}
