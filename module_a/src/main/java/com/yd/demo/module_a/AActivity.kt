package com.yd.demo.module_a

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.yd.demo.arouter.ARouter

class AActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.module_a_activity_a)

        findViewById<Button>(R.id.jump_btn).setOnClickListener {
            ARouter.getInstance().build("/module/b").navigation(this@AActivity);
        }
    }
}
