package com.yd.demo.module_b

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.yd.demo.arouter.ARouter

class BActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.module_b_activty_b)

        findViewById<Button>(R.id.jump_btn).setOnClickListener {
            ARouter.getInstance().build("/module/a").navigation(this@BActivity);
        }
    }
}
