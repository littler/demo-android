package com.yd.demo.arouter;

import android.content.Context;
import android.content.Intent;

import java.util.HashMap;
import java.util.Map;

/**
 * 两个没有任何依赖的模块module_a和module_b通过共同的依赖模块arouter进行相互间的通信
 * <p>
 * 简单实现阿里路由框架ARouter的路由功能
 * <p>
 * 原以为路由模块也需要依赖模块A和模块B，这样才能通过startActvity(this, moduleA.class)/startActvity(this, moduleB.class)跳转到对应的模块，
 * 但发现阿里的ARouter并不需要ARouter模块去反向依赖模块啊，其实原因是只需要模块A和模块B将各自的class对象传给ARouter即可
 */
public class ARouter {

    private static ARouter sInstance;

    // 构造器私有
    private ARouter() {

    }

    public static ARouter getInstance() {
        if (sInstance == null) {
            synchronized (ARouter.class) {
                if (sInstance == null) {
                    sInstance = new ARouter();
                }
            }
        }

        return sInstance;
    }

    private String uri;
    private Map<String, Class> map = new HashMap<>();

    public void add(IIndex index) {
        map.put(index.getPath(), index.getClaz());
    }

    public ARouter build(String uri) {
        this.uri = uri;
        return this;
    }

    public void navigation(Context context) {

        Class claz = map.get(uri);

        if (claz != null) {
            context.startActivity(new Intent(context, claz));
        }
    }

}
